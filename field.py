from cell import Cell

class Field:

    __slots__ = ("__initial",)

    def __init__(self):
        self.__initial = Cell()

    def get_initial(self):
        return self.__initial
