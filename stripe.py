from cell import Cell
from utils.dicts import reverse_dict


class Stripe:

    def __init__(self, direction : str, connection : str, source : Cell, length : int):
        self.__direction = direction
        self.__connection = connection
        self.__source = source
        self.__length = length
        self.__curr_cell = Cell()

    def get_stripe(self):
        
        rev_connection = reverse_dict[self.__connection] # get reverse connection
        new_cell = Cell() # create new cell
        new_cell.set(self.__connection, self.__source) # set stripe source neighbour
        self.__source.set(rev_connection, new_cell) # connect new_cell with source
        self.__curr_cell = new_cell
        for _ in range(self.__length-1): # complete the stripe with new cells
            new_cell = Cell()
            new_cell.set(reverse_dict[self.__direction], self.__curr_cell)
            self.__curr_cell.set(self.__direction, new_cell)
            self.__curr_cell = new_cell
            print(self.__curr_cell)
        
    
    def get_curr_cell(self):
        return self.__curr_cell

    def __len__(self):
        return self.__length
    
    def __str__(self):
        string = ''
        current = self.__curr_cell
        rev_direction = reverse_dict[self.__direction]
        while current:
            string += str(current)
            current = current.get(rev_direction)
        
        return string
            

def main():
    s = Stripe('r', 'u', Cell(), 10)
    print(s)

if __name__=="__main__":
    main()

