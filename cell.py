
class Cell:

    __slot__ = ('__left', '__right', '__up', '__down', '__filling')

    def __init__(self, left=None, right=None, up=None, down=None, filling='£'):
        self.__left = left
        self.__right = right
        self.__up = up
        self.__down = down
        self.__filling = filling 
        self.__set_dict, self.__get_dict = self.create_dict()

    def set(self, direct : str, neighbour):
        self.__set_dict[direct](neighbour)
    
    def get(self, direct : str):
        return self.__get_dict[direct]()
        
    def create_dict(self):
        set_dict = {
            'l' : self.__set_left,
            'r' : self.__set_right,
            'u' : self.__set_up,
            'd' : self.__set_down
        }

        get_dict = {
            'l' : self.__get_left,
            'r' : self.__get_right,
            'u' : self.__get_up,
            'd' : self.__get_down
        }
        return set_dict, get_dict

    def __set_up(self, up):
        self.__up = up

    def __get_up(self):
        return self.__up

    def __set_down(self, down):
        self.__down = down

    def __get_down(self):
        return self.__down

    def __set_left(self, left):
        self.__left = left

    def __get_left(self):
        return self.__left

    def __set_right(self, right):
        self.__right = right

    def __get_right(self):
        return self.__right

    def set_filling(self, filling):
        self.__filling = filling
    
    def get_filling(self):
        return self.__filling

    def __str__(self):
        return self.__filling

def main():
    c = Cell(filling='3')
    c.set('u', Cell())
    c.set('d', Cell())
    c.set('l', Cell())
    c.set('r', Cell())
    print(' ' ,c.get('u') ,' ')
    print(c.get('l') ,c , c.get('r'))
    print(' ' ,c.get('d') ,' ')

if __name__ == "__main__":
    main()