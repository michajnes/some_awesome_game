class Motion:
    
    @classmethod
    def left(cls, obja):
        print(f"{obja.__class__.__name__} moves left")
    
    @classmethod
    def right(cls, obja):
        print(f"{obja.__class__.__name__} moves right")

    @classmethod
    def up(cls, obja):
         print(f"{obja.__class__.__name__} moves up")
    
    @classmethod
    def down(cls, obja):
        print(f"{obja.__class__.__name__} moves down")