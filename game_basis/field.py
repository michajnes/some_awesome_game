class FieldFilling:
    util_dict = {
        0: (-1, -1),
        1: (-1, 0),
        2: (-1, 1),
        3: (0, 1),
        4: (1, 1),
        5: (1, 0),
        6: (1, -1),
        7: (0, -1)
        }

    @classmethod
    def place_heroe(cls, field, heroe):
        HEROE_CHAR = "\u29D3"
        
        x, y = heroe.coords
        field[y][x].character = HEROE_CHAR
        field[y][x].value = heroe


    @classmethod
    def draw_field(cls, field, size):
        for y in range(size[0]):
            s = " ".join([str(field[y][x].character) for x in range(size[1])])
            print(s)