from node import Node

class Stack:

    def __init__(self):
        self.top = None
    
    def push(self, value):
        if self.is_empty():
            self.top = Node(value)
        else:
            self.top = Node(value, self.top)
    
    def pop(self):
        if self.is_empty():
            print("Stack is empty")
            return
        value = self.top.value
        self.top = self.top.prev
        return value

    def clear(self):
        self.top = None
    
    def is_empty(self):
        return self.top == None

def main():
    s = Stack()
    for i in range(10):
        s.push(i)
    for i in range(10):
        print(s.pop())


if __name__ == "__main__":
    main()

if __name__ == "stack":
    print("We are named STACK!!!")

