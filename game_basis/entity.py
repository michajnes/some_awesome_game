class Entity:
    def __init__(self, value=None, char="\u041E", neighbours=None):
        self.value = value
        self.character = char
        if isinstance(neighbours, (list, tuple)):
            n = len(neighbours)
            if n < 8:
                self.neighbours = neighbours + [None for _ in range(8 - n)]
            elif n >= 8:
                self.neighbours = [neighbours[i] for i in range(8)]
    
    def __str__(self):
        return self.character

    def __repr__(self):
        return str(self)
