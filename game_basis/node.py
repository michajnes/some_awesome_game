class Node:

    def __init__(self, value=None, prev=None, next=None):
        self.value = value
        self.prev = prev
        self.next = next
    
if __name__ == "__main__":
    n = Node(0)
    curr_node = n
    for i in range(1, 11):
        curr_node.next = Node(i)
        curr_node = curr_node.next
    
    print(n.next.next.next.next.next.next.next.next.value)
