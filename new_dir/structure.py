import os

class Node:
    """Node class is the base of basic structures such as queue, stack, tree etc."""

    def __init__(self, value=None, next_node=None, prev_node=None):
        self.next_node = None
        self.prev_node = None

        if next_node != None and isinstance(next_node, Node):
            self.next_node = next_node

        if prev_node != None and isinstance(prev_node, Node):
            self.prev_node = prev_node

        self.value = value

class Stack:

    def __init__(self):
        self.top = None
    
    def push(self, value):
        if self.top == None:
            self.top = Node(value)
        else:
            self.top = Node(value, self.top)
    
    def pop(self):
        if self.top == None:
            print("The stack is already empty!")
            return
        else:
            value = self.top.value
            self.top = self.top.next_node
            print(f"Deleted value: {value}")
            return value

    def display(self):
        curr_node = self.top
        while curr_node != None:
            print(curr_node.value, end="->")
            curr_node = curr_node.next_node
    
        print("None")

def main():
    os.system('clear')
    st = Stack()
    
    for i in range(10):
        st.push(i)
    
    st.display()
    st.pop()
    st.pop()
    st.display()

    
    while True:
        s = input("Choose action (p, d, s, e): ")
        if s == "p":
            value = int(input("Enter the value: "))
            st.push(value)
        elif s == "d":
            st.pop()
        elif s == "s":
            st.display()
        elif s == "e":
            break
        else:
            print("Choose the letter from list!")

        

if __name__ == "__main__":

    main()