from tracemalloc import start
from new_dir.structure import Node


class Monomial(Node):
    def __init__(self, powers=list(), value=None, next_node=None, prev_node=None):
        self.powers = powers
        super.__init__(value, next_node, prev_node)
    
    def __str__(self):
        return

class Polynomial:

    def __init__(self):
        self.head = None
        self.tail = self.head
    
    def __str__(self):
        if self.head == self.tail == None:
            return "0"
        elif self.head == self.tail and isinstance(self.head, Monomial):
            return str(self.head)