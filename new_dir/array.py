import os
from math import inf
os.system('clear')

def create_array(n : int, t : type):
    
    arr = [0 for i in range(n)]
    return arr

def print_arr(arr):
    for i in range(len(arr)):
        print(arr[i], end=" ")

    print()

def input_arr(arr):
    for i in range(len(arr)):
        arr[i] = int(input(f"Enter arr[{i}]:"))

    print()

def swap(arr, i, j):
    """Swap values of ith and jth elements"""

    temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp 

def summ(arr): # вивчити!!!
    """Return the sum of all array elements values"""

    n = len(arr)
    s = 0

    for i in range(n):
        s += arr[i] 

    return s

def prod(arr): # вивчити!!!
    """Return the product of all array elements values"""

    n = len(arr)
    p = 1

    for i in range(n):
        p *= arr[i]

    return p

def arr_min(arr): # вивчити!!!

    n = len(arr)
    m = arr[0] # positive infinity 
    for i in range(1,n):
        if arr[i] < m:
            m = arr[i]
    return m

def arr_max(arr): # вивчити!!!
    
    n = len(arr)
    m = arr[0]
    for i in range(1,n):
        if m < arr[i]:
            m = arr[i]
    return m


def reverse(arr):
    n = len(arr)
    for i in range(n // 2):
        swap(arr, i, n - 1 - i) 

def insert_sort(arr):
    # (4)[5],6,2,1
    # (4,5)[6],2,1
    # (4,5,6)2,1
    # (4,5,6,2)1
    # (4,5,2,6)1
    # (4,2,5,6)1
    # (2,4,5,6)1
    # (1,2,4,5,6)
    for i in range(1, len(arr)):
        key_el = arr[i]
        j = i - 1
        while j >= 0 and key_el < arr[j]:
            arr[j + 1] = arr[j]
            j -= 1
            print_arr(arr)

        arr[j + 1] = key_el


def bubble_sort(arr):
    
    n = len(arr)

    for i in range(n):

        for j in range(n-i-1):
            if arr[j] > arr[j + 1]:
                swap(arr, j, j + 1)

        # 4,5,6,2,1
        # 4,5,2,6,1
        # 4,5,2,1,6
        # 4,2,5,1,6
        # 4,2,1,5,6
        # 2,4,1,5,6
        # 2,1,4,5,6
        # 1,2,4,5,6

def select_sort(arr):
    n = len(arr)
    for i in range(n-1):
        min_idx = i
        for j in range(i+1, n):
            if arr[min_idx] > arr[j]:
                min_idx = j
        swap(arr, min_idx, i) # arr[min_idx], arr[i] = arr[i], arr[min_idx]
        print(min_idx, " ", end=": ")
        print_arr(arr)

def main():
    try:
        arr = create_array(7, int)
        print_arr(arr)
        input_arr(arr)
        print_arr(arr)
        print(f"Sum of arr els = {summ(arr)}")
        print(f"Prod of arr els = {prod(arr)}")
        # Task 1: написати алгоритм реверсу масиву, тобто усі елементи у тому самому порядку тільки з кінця 
        # наприклад: було [2, 7, -1 , 3, 10], а має стати [10, 3, -1, 7, 2]
        print()
        #reverse(arr)
        #print()
        # Task цікаве: Напиши алгоритм сортування 5 елементного масиву arr

        # TODO
        #select_sort(arr)
        #print_arr(arr)

    except(IndexError):
        print("IndexError occured")



if __name__ == "__main__":
    main()





