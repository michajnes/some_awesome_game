import os

class HashSet:

    def __init__(self):
        self.set_dict = dict()
        self.set_list = list()
    
    def _hash(self, value):
        if not isinstance(value, (list, dict, set)):
            return value.__hash__()
        else:
            s = str(value)
            return str(s.__hash__()) + str(type(value))
    
    def add(self, value):
        if not self.collision(value):
            self.set_dict[self._hash(value)] = list()
            self.set_list.append(value)
            
        self.set_dict[self._hash(value)].append(value)

    def collision(self, value):
        if value in self.set_list or self._hash(value) in self.set_dict.keys():
            return True
        return False
    
    def __str__(self):
        s = "{"
        for el in self.set_list:
            if isinstance(el, str):
                s += "\'" + el + "\'" + ", "
            else:
                s += str(el) + ", "
        return s[:-2] + "}"

def main():
    a = HashSet()
    a.add(3)
    a.add(3)
    a.add(5)
    a.add(6)
    a.add(3)
    a.add({'3':56, 'straus': 'kakadu'})
    a.add(5)
    a.add(3)
    a.add(3)
    a.add(5)
    a.add("234")
    a.add(3)
    a.add(3)
    a.add(5.1)
    a.add("234")
    a.add((2,3))
    a.add([3,4,5])
    print(a)        

if __name__== "__main__":
    os.system('clear')
    main()

    # int - ціле число: 4, -36, 0 
    # float - дробове число: 3.2, -7.5
    # str, list, tuple, int, float, set, dict, bool
    # str - рядок: "abc" (immutable)
    # list - список: [a,a,c] - є порядок (mutable)
    # tuple - мультиплет: (a, b, b, c) - є порядок (immutable)
    # set - множина: {a, b, c} - порядку нема (mutable)
    # dict - словник: {'a':3, 'b': 'green', 'Viktor': '(067)345-73-22'} (mutable)
