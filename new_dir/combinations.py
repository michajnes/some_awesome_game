    # |2|123|45|-3|0|11111111|45|-78|21|73|-89|61|47|71|11|;
    # ->>|********|->>return 
    # ->>|*&*&&&**|
    # |*&**&**&|->>return 
    # |&&&****&&|
    # ГГГ, ГЧГ, ЧГГ, ЧЧГ, ГГЧ, ГЧЧ, ЧГЧ, ЧЧЧ, РГГ, РГЧ, РГР, 
    # РЧГ, РЧЧ, РЧР, РРГ, РРЧ, РРР, ГГР,ГЧР,ГРР, ЧГР,
    # ЧЧР, ЧРР, ГРГ, ГРЧ, ЧРГ, ЧРЧ
    # 4 * 4 * 4 * 4 * 4 * 4 * 4

    # 0! = 1! / 1 = 1
    # 1! = 1 = 1 : 1
    # 2! = 1*2 = 2 : 12, 21     у 2 рази
    # 3! = 1*2*3 = 6 : 123, 132, 213, 231, 312, 321 у 3 рази 
    # 4! = 1*2*3*4 = 24 : 1234, 1243, 1324, 1342, 1423, 1432, 2134, 2143, 2314, 2341, 2413, 2431, ... у 4 рази
    # 5! = 1*2*3*4*5 = 120    у 5 разів
    # 6! = 1*2*3*4*5*6 = 720
    # 7! = 7 * 6! = 5040
    # 8! = 8 * 7! = 40320
    # 9! = 9 * 8! = 362880
    # n! = n * (n-1)!
    # широта

def fac(n : int):
    if n < 0:
        raise ValueError(f"Value n = {n} is not allowed!")
    elif n == 0:
        return 1
    else:
        return n * fac(n - 1) 

def f(n : int):
    l = [1, 1] + [0 for _ in range(n-1)]
    for i in range(1,n+1):
        l[i] = l[i - 1] * i
    return l[n]

def permutation(string : list):
    n = len(string)
    l = []

    if n == 0 or n == 1:
        l.append(string)
        return l

    else:
        for ch in string:
            new_str = string.replace(ch, "")
            new_l = permutation(new_str)

            for s in new_l:
                l.append(ch+s)

        return l

def main():
    try:
        s = input("Enter the string: ")
        print(len(permutation(s)))
    except(ValueError):
        print("Some ValueError")
    finally:
        print()
        print("The end!")
        print()

if __name__ == "__main__":
    print(f(5))
    # main()

